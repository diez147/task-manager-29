package ru.tsc.babeshko.tm.exception.entity;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}