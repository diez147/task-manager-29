package ru.tsc.babeshko.tm.exception.system;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}