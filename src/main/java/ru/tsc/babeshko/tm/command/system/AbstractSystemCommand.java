package ru.tsc.babeshko.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.command.AbstractCommand;
import ru.tsc.babeshko.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}